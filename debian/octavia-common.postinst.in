#!/bin/sh

set -e

CONF=/etc/octavia/octavia.conf

#PKGOS-INCLUDE#


write_octavia_admin_creds () {
	# This is "forked" pkgos_write_admin_credentials function
	# as octavia need to have service_auth block filled in config
	#
	# This function is doing the same as pkgos_write_admin_creds but
	# filling also service_auth
	local WRITE_CRED_CONF_FNAME WRITE_CRED_PKG_NAME WRITE_CRED_SECTION NO_PROTO AFTER_PORT WRITE_CRED_URL WRITE_CRED_PROTO
	WRITE_CRED_CONF_FNAME=${1}
	WRITE_CRED_SECTION=${2}
	WRITE_CRED_PKG_NAME=${3}

	db_get ${WRITE_CRED_PKG_NAME}/configure_ksat
	if [ "${RET}" = "true" ] ; then
		db_get ${WRITE_CRED_PKG_NAME}/ksat-public-url
		KEYSTONE_PUBLIC_ENDPOINT=${RET}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} ${WRITE_CRED_SECTION} www_authenticate_uri ${KEYSTONE_PUBLIC_ENDPOINT}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} ${WRITE_CRED_SECTION} auth_uri ${KEYSTONE_PUBLIC_ENDPOINT}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} ${WRITE_CRED_SECTION} auth_url ${KEYSTONE_PUBLIC_ENDPOINT}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} service_auth auth_url ${KEYSTONE_PUBLIC_ENDPOINT}
		db_get ${WRITE_CRED_PKG_NAME}/ksat-region
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} ${WRITE_CRED_SECTION} region_name ${RET}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} service_auth region_name ${RET}

		db_get ${WRITE_CRED_PKG_NAME}/ksat-service-username
		SERVICE_USERNAME=${RET}
		db_get ${WRITE_CRED_PKG_NAME}/ksat-service-project-name
		SERVICE_PROJECTNAME=${RET}
		db_get ${WRITE_CRED_PKG_NAME}/ksat-service-password
		SERVICE_PASSWORD=${RET}

		# Should we create a new keystone user?
		db_get ${WRITE_CRED_PKG_NAME}/ksat-create-service-user
		if [ "${RET}" = "true" ] ; then
			# Set command line credentials
			db_get ${WRITE_CRED_PKG_NAME}/ksat-admin-username
			export OS_USERNAME=${RET}
			db_get ${WRITE_CRED_PKG_NAME}/ksat-admin-project-name
			export OS_TENANT_NAME=${RET}
			export OS_PROJECT_NAME=${RET}
			db_get ${WRITE_CRED_PKG_NAME}/ksat-admin-password
			export OS_PASSWORD=${RET}
			export OS_AUTH_URL=${KEYSTONE_PUBLIC_ENDPOINT}
			export OS_IDENTITY_API_VERSION=3
			export OS_PROJECT_DOMAIN_ID=default
			export OS_USER_DOMAIN_ID=default
			export OS_AUTH_TYPE=password

			# And create that new service user with role admin
			echo "Creating project ${SERVICE_PROJECTNAME} ..."
			openstack project create --or-show ${SERVICE_PROJECTNAME} --description "Debian service project"

			echo "Creating user ${SERVICE_USERNAME} ..."
			openstack user create --or-show --password ${SERVICE_PASSWORD} --project ${SERVICE_PROJECTNAME} --email root@localhost --enable ${SERVICE_USERNAME}

			# This should not be needed right now, so removing it...
			# Edit: This is not truth, admin role is needed, it is also documented in documentation {kevko}
			echo "Adding role admin to the user ${SERVICE_USERNAME}"
			openstack role add --project ${SERVICE_PROJECTNAME} --user ${SERVICE_USERNAME} admin
			# Edit : This is bad >> {kevko}
			#echo "Adding role service to the user ${SERVICE_USERNAME} ..."
			#openstack role add --project ${SERVICE_PROJECTNAME} --user ${SERVICE_USERNAME} service
		fi
		echo "===> opensatck-pkg-tools: writing ${WRITE_CRED_SECTION} credentials for user ${SERVICE_USERNAME} and project ${SERVICE_PROJECTNAME} ..."
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} ${WRITE_CRED_SECTION} username ${SERVICE_USERNAME}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} ${WRITE_CRED_SECTION} project_name ${SERVICE_PROJECTNAME}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} ${WRITE_CRED_SECTION} password ${SERVICE_PASSWORD}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} service_auth username ${SERVICE_USERNAME}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} service_auth project_name ${SERVICE_PROJECTNAME}
		pkgos_inifile set ${WRITE_CRED_CONF_FNAME} service_auth password ${SERVICE_PASSWORD}
	fi
	# For security reasons, we don't keep the auth-token in the debconf
	# memory, so we purge it with db_unregister.
	db_unregister ${WRITE_CRED_PKG_NAME}/ksat-admin-password
	# Very likely, we don't want any upgrade to touch an already
	# working value of keystone_authtoken, so it's best to just
	# unregister this.
	db_unregister ${WRITE_CRED_PKG_NAME}/configure_ksat
}

if [ "$1" = "configure" ] || [ "$1" = "reconfigure" ] ; then
	. /usr/share/debconf/confmodule
	. /usr/share/dbconfig-common/dpkg/postinst

	pkgos_var_user_group octavia /bin/sh

	# Create config files if they don't exist
	pkgos_write_new_conf octavia octavia.conf
	if [ -r /etc/octavia/policy.json ] ; then
		mv /etc/octavia/policy.json /etc/octavia/disabled.policy.json.old
	fi

        db_get octavia/configure_db
        if [ "$RET" = "true" ]; then
                pkgos_dbc_postinst ${CONF} database connection octavia $@
                echo "Now doing octavia-db-manage upgrade head: this may take a while..."
		su octavia -s /bin/sh -c "octavia-db-manage upgrade head"
        fi

        pkgos_rabbit_write_conf ${CONF} oslo_messaging_rabbit octavia
        write_octavia_admin_creds ${CONF} keystone_authtoken octavia

fi

#DEBHELPER#

exit 0
